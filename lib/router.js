Router.configure({
    // we use the  appBody template to define the layout for the entire app
    layoutTemplate: 'layout',

    loadingTemplate: 'spinner',

});



Router.map(function() {

    this.route('/', function() {
        this.render('signin');
    });

    this.route('/signin', function() {
        this.render('signin');
    });


    this.route('/home', function() {
        this.render('home');
    });

    this.route('/users', function() {
        this.render('users', {data: function(){return {UsersInfo: UserList.find()}}});
    });

    this.route('/appusers', function() {
        this.render('waygroup', {data: function(){return {UsersInfo: UserList.find()}}});
    });

    //  this.route('/registerWebUser', function() {
    //     this.render('registerWebUser');
    // });

    this.route('/registerOptions', function(){

        //this.render('registerOptions');
        // }
        // }
        this.render('registerOptions');
        // controller: 'RegisterOptionsController',
        // action: 'doSomething'

      


    });

    this.route('/registerWebUser', function() {
        this.render('registerWebUser');
        // onBeforeAction: function() {
        //     // if (!Meteor.userId()) {
        //     //     this.render('signin');
        //     // } else {
        //     //     this.next();
        //     // }
        // }
    });

    // this.route('/registerWebUser', function() {
    //     this.render('registerWebUser');
    // });

    this.route('/registerMobileUser', function() {
        this.render('registerMobileUser');

    });

    // this.route('/registerOptions', function() {
    //     this.render('registerOptions'),
    //         onBeforeAction: function() {
    //             if (!Meteor.userId()) {
    //                 this.render('signin');
    //             } else {
    //                 this.next();
    //             }
    //         }
    // });


});


// RegisterOptionsController = RouteController.extend({


//     onBeforeAction: function() {
//         console.log('app before Action is called');
//         // this.next(); 
//         if (isUserLoggedIn) {
//             this.next;
//         } else {
//             Router.go('signin');
//         }

//     },

//     doSomething: function() {
//         console.log('doSomething action is called!');
//         if(username == 'admin')
//             this.render('registerOptions');
//         else
//             this.render('home');
//     }
// });
