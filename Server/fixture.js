Meteor.startup(function() {
    if (Meteor.users.find().count() === 0) {
        Accounts.createUser({
            username: 'admin',
            password: 'admin',
            profile: {
                first_name: 'profileFirstName',
                last_name: 'profileLastName',
                company: 'anyData',
            }
        });
    }
});
