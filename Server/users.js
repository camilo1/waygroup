//UserList.remove({});
Meteor.methods({

	'insertUser': function(username,password,usertype,owner){
		var tDate = new Date();

		UserList.insert({			
			UserName: username,
            Pwd: password,
            Type: usertype,
            Owner: owner,
			createdAt: tDate
		});
	},

	'delUser_For_Admin_User': function(sID){

		UserList.remove(sID);

	},

	'updateUser': function(username, usertype){
		// console.log(usertype);
		UserList.update({UserName: username},{$set: {Type: usertype}});

	}

});