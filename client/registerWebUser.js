function createDatabaseUser(e, t) {
    //     userType = t.find('#userType').value,
    //     webUserName = t.find('#webUserName').value,
    //     webPassword = t.find('#webPassword').value;

    // // Trim and validate the input
    // console.log("UserName:" + userName);
    // console.log("Password:" + password);
    // console.log("UserType:" + userType);
    // console.log("webUserName:" + webUserName);
    // console.log("webPassword:" + webPassword);

    // Accounts.createUser({username: userName, password : password, usertype:userType, 
    //   webusername:webUserName, webpassword: webPassword},
    //    function(err){
    //     if (err) {
    //       // Inform the user that account creation failed
    //       console.log("Error while creating new user: " + err);
    //     } else {
    //       // Success. Account has been created and the user
    //       // has logged in successfully.
    //       console.log("User Successfully created"); 
    //     }

    //   });
}
 Session.set("error", false);
// function isFormValid(username, password) {
//     if ((password.length >= 1) && (username.length >= 1)) {
//         Session.set("error", false)
//         return true;
//     } else {
//         Session.set("error", true);
//         return false;
//     }
// }
Template.registerWebUser.rendered = function()
{
     Session.set("error",false);
     Session.set("success",false);
     Session.set("loading",false);
     Session.set("currentPage", "RegisterWebUser");
}
  

Template.registerWebUser.helpers({


    error: function() {
        return Session.get("error");
    },

    success:function()
    {
        return Session.get("success");
    },

    loading:function()
    {
        return Session.get("loading");
    }

});

function registerWebUser(username,password)
{
    Session.set("loading",true);
    
     var url = usersUrl;
            HTTP.post(url, {
                    headers: {
                        'X-Parse-Application-Id': appID,
                        'X-Parse-REST-API-Key': restKey,
                        'content-Type': 'application/x-www-form-urlencoded'
                    },
                    params: {
                        'username': username,
                        'password': password,
                        'usertype': "3",

                    }
                }, function(error, result) {
                    Session.set("loading",false);
                    if (error) {

                        console.log(error);
                        Session.set("error",true);

                    } else {

                        Meteor.call('insertUser', username, password, '3', '');

                        document.getElementById("formRegisterWeb").reset();
                        Session.set("success",true);

                    }
                }

            );
}

Template.registerWebUser.events({
    'submit #formRegisterWeb': function(e, t) {
        e.preventDefault();

        Session.set("error",false);

        var username = t.find("#username").value,
            password = t.find("#password").value;

        var isValid = isFormValid(username, password);
        if (isValid) {

            if(!Session.get("loading"))
                registerWebUser(username,password);
           
        } else {
            Session.set("error",true);
        }

        return false;

    }
});
