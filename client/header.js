//console.log("Current Route: " + Router.current());
Template.header.helpers({

    // trim helper
    headerTitle: function(val) {
        if(Session.get("currentPage") == "home")
             return "REPORTE";
        else  if(Session.get("currentPage") == "RegisterOptions")
            return "Nuevos Usuarios: web ó móvil";
        else  if(Session.get("currentPage") == "RegisterWebUser")
            return "Create a new Web User";
        else  if(Session.get("currentPage") == "RegisterMobileUser")
            return "Create a new Mobile User";
        else  if(Session.get("currentPage") == "allUser")
            return "Todos los usuarios";
        else  if(Session.get("currentPage") == "myUser")
            return "USUARIOS";

        return "Welcome to Waygroup";
    },

    // length greater than 6
    isValidPassword: function(val, field) {
        // if (val.length > 6) {
        //     return true;
        // } else {
        //     Session.set('displayMessage', 'Error &amp; Too short.')
        //     return false;
        // }
        return true;
    }
});


Template.signin.rendered = function() {
   
    Session.set("currentPage", "header");

}