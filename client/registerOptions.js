Template.registerOptions.events({
    'click #webUserBtn': function(e, t) {
        Router.go('registerWebUser');
    },

    'click #mobileUserBtn': function(e, t) {
        Router.go('registerMobileUser');
    },

    'click #allUserBtn': function(e, t) {
        Router.go('appusers');
    }
});

Template.registerOptions.rendered = function()
{
   
     Session.set("currentPage", "RegisterOptions");
}
