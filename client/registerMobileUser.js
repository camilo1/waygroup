var usersArr = new ReactiveVar();
//console.log("Registered mobile File Start");

Template.registerMobileUser.created = function() {
    //console.log("Registered mobile Template Created");

}

Template.registerMobileUser.helpers({


    getOwners: function() {
        //console.log("Owners: " + usersArr);
        return usersArr.get();
    },

    createNewUser: function(e, t) {
        console.log("Create new user is called");
    },

    title: function() {
        return Session.get("title");
    },
    error: function() {
        return Session.get("error");
    },
    success: function() {
        return Session.get("success");
    },
    loading: function()
    {
        return Session.get("loading");
    }

});
/*
function getOwnersFunction() {
    var url = "https://api.parse.com/1/users";
    var result = HTTP.get(url, {
        headers: {
            'X-Parse-Application-Id': appID,
            'X-Parse-REST-API-Key': restKey,
            'content-type': 'application/json'

        }
        // query: 'where={"owner":"owner125"}'
    }, function(error, result) {

        if (error) {
            console.log("RegisterMobileUserError: " + error.content);
            //   Session.set("error", true);
        } else {
            console.log("Mobile User created: " + result.content);
            //  Session.set("success", true);
        }

        // if (error)
        //     console.log("Error: " + error);
        // else {
        //     console.log("Response Received");
        //     // var data = result.results;
        //     // console.log("Data: " + data);
        //     // UserArr.set(JSON.parse(data));
        //     console.log("Result: " + JSON.parse(result));
        // }
    });

}
*/
// Meteor.autorun(function() {
//     console.log("AutoRun  UserArr: " + usersArr);
// });


function createNewUser(username, password, usertype, owner){

        Session.set("loading",true);

            //var url = 'https://api.parse.com/1/users';
            var url = usersUrl;
            HTTP.post(url, {
                    headers: {
                        'X-Parse-Application-Id': appID,
                        'X-Parse-REST-API-Key': restKey,
                        'content-Type': 'application/x-www-form-urlencoded'
                    },
                    params: {
                        'username': username,
                        'password': password,
                        'usertype': usertype,
                        'owner': owner

                    }
                }, function(error, result) {

                    Session.set("loading",false);

                    if (error) {

                        console.log(error);
                        Session.set("error", true);
                        Session.set("success", false);

                    } else {

                        Meteor.call('insertUser', username, password, usertype, owner);

                        Session.set("success", true);
                        Session.set("error", false);
                        
                        document.getElementById("register-form").reset();
                    }
                }

            );
      
  

}

function fetchOwners() {
    //var url = "https://api.parse.com/1/users";
    var url = usersUrl;
    HTTP.get(url, {
            headers: {
                'X-Parse-Application-Id': appID,
                'X-Parse-REST-API-Key': restKey,
                'content-type': 'application/json'

            }
            ,
            query: 'where={"usertype":"3"}',
            params:{
               limit:'900'  
            }
           

            // query: 'where={"owner":"owner125"}'
        }, function(error, result) {
            //console.log("Content: " + result.content);
            //debugger;
            if (error)
                console.log("Error: " + error);
            else {
                var parsedContent = JSON.parse(result.content);
                var results = parsedContent.results;
                console.log("Result: " + result);
                console.log("Length:" + results.length);
                console.log("Result Conent: " + result.content);
                 //console.log("Length:" + result.content.length);
                console.log("ParsedContent: " + parsedContent);
                usersArr.set(results);
            }

        }


    );
}


Template.registerMobileUser.rendered = function() {
    Session.set("error", false);
    Session.set("success", false);
    Session.set("loading",false)
    Session.set("currentPage", "RegisterMobileUser");
    //console.log("Template registerMobileUser is rendered");

    fetchOwners();
    //getOwnersFunction();
}

Template.registerMobileUser.events({
            'submit #register-form': function(e, t) {


                e.preventDefault();

                var username = t.find('#username').value,
                    password = t.find('#password').value,
                    usertype = t.find('#usertype').value,
                    owner = t.find('#owner').value;

                var isValid = isFormValid(username, password);

                if (isValid) {
                    if (owner.length >= 1){

                        if(!Session.get("loading"))
                            createNewUser(username, password, usertype, owner);
                    }
                } else {

                    Session.set("error", true);
                    Session.set("success", false);
                }

                    return false;
                },

                'click .testuser': function(){
                    //console.log(this);
                }
            });

        // Accounts.createUser({username: userName, password : password, usertype:userType, 
        //   webusername:webUserName, webpassword: webPassword},
        //    function(err){
        //     if (err) {
        //       // Inform the user that account creation failed
        //       console.log("Error while creating new user: " + err);
        //     } else {
        //       // Success. Account has been created and the user
        //       // has logged in successfully.
        //       console.log("User Successfully created"); 
        //     }

        //   });
