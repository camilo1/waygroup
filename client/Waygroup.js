Template.waygroup.rendered = function(){
	Session.set("UserType", "");
    Session.set("currentPage", "allUser");
}

Meteor.autorun(function(){
    Meteor.subscribe('Directory', Cookie.get('loggedInUser'), Session.get('UserType'));
});

Template.waygroup.helpers({
	dataExist: function(){
		if(UserList.find().count() > 0)
			return true;
		else
			return false;
    },
});

Template.waygroup.events({
    'change .filterUser': function(){
        Session.set("UserType", $('#filterUser').val());
    },

    'click .rmUser': function(){
        //Meteor.call('delUser_For_Admin_User', this._id);
    },
});