Template.users.rendered = function() {
    Session.set("Navigation", "user");
    Session.set("UserType", "");
    Session.set("currentPage", "myUser");
}

Template.users.helpers({
	dataExist: function(){
		if(UserList.find().count() > 0)
			return true;
		else
			return false;
    },
    One: function(){
      if(Session.get('UserType') === '1'){
            return 'selected';
      }else{
        return '';
      }
    },
    Two: function(){
      if(Session.get('UserType') === '2'){
            return 'selected';
      }else{
        return '';
      }
    },
    Both: function(){
      if(Session.get('UserType') === ''){
            return 'selected';
      }else{
        return '';
      }
    },
});

Template.users.events({
  
    'change .filterUser': function(){
        Session.set("UserType", $('#filterUser').val());
    },

});