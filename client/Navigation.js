Template.navigator.helpers({
    navReport: function(){
      if(Session.get("Navigation") === 'report'){
          return 'active';
      }else{
          return '';
      }
    },
    navUser: function(){
      if(Session.get("Navigation") === 'user'){
          return 'active';
      }else{
          return '';
      }
    },
});