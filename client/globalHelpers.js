isFormValid = function(username, password) {
    if ((password.length >= 1) && (username.length >= 1)) {

        return true;
    } else {

        return false;
    }
}


replaceComma = function(stringVar)
{

    var str = stringVar.replace(/\n|,/g, " ");
    return str;
}

//Handlebars.registerHelper('replaceComma', replaceComma);


exportCSV = function(responseStream) {

    
     //console.log("Test 1");
    var userStream = createStream();
    // Set up a future, Stream doesn't work properly without it.
    var fut = new Future();
    var users = {};

     console.log("Test 2");
    //Here this Package is used to parse a stream from an array to a string of CSVs.
    CSV().from(userStream)
        .to(responseStream)
        .transform(function(user, index) {
                // if(user._id){
                //     var dateCreated = new Date(user.createdAt);
                //     return [user.profile.name, user.emails[0].address, dateCreated.toString()];
                // }else
                //     return user;
                // }
                 console.log("Test 3");
                return user;
            }

        )
        .on('error', function(error) {
           
     	console.log("Test 4");
            console.error('Error streaming CSV export: ', error.message);
        })
        .on('end', function(count) {
           
    		 console.log("Test 5");
            responseStream.end();
            fut.return();
        });

    //Write table headings for CSV to stream.
    userStream.write("Name");

    //users = ["user1", "user2", "user3", "user4", "user5"];

    //Pushing each user into the stream, If we could access the MongoDB driver we could
    //convert the Cursor into a stream directly, making this a lot cleaner.
    users.forEach(function(user) {
        userStream.write(user); //Stream transform takes care of cleanup and formatting.
        count += 1;
        if (count >= users.count())
            userStream.end();
    });

    return fut.wait();
};

//Creates and returns a Duplex(Read/Write) Node stream
//Used to pipe users from .find() Cursor into our CSV stream parser.
var createStream = function() {
    var stream = Npm.require('stream');
    var myStream = new stream.Stream();
    myStream.readable = true;
    myStream.writable = true;

    myStream.write = function(data) {
        myStream.emit('data', data);
        return true; // true means 'yes i am ready for more data now'
        // OR return false and emit('drain') when ready later
    };

    myStream.end = function(data) {
        //Node convention to emit last data with end
        if (arguments.length)
            myStream.write(data);

        // no more writes after end
        myStream.writable = false;
        myStream.emit('end');
    };

    myStream.destroy = function() {
        myStream.writable = false;
    };

    return myStream;
};
