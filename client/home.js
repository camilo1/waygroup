//console.log("Home Page");
Session.set("currentPage", "home");
Session.setDefault("UserType", "");

Meteor.autorun(function() {
    Meteor.subscribe('NotesList');
    Meteor.subscribe('Directory', Cookie.get('loggedInUser'), Session.get('UserType'));
});

var note = replaceComma("This, is, the, notes Text Placeholder   ");

//console.log("Note: " + note.trim());
var imageData = new ReactiveVar();
// Meteor.call('fetchFromService',  function(err, respJson) {
//     if (err) {
//         window.alert("Error: " + err.reason);
//         console.log("error occured on receiving data on server. ", err);
//     } else {
//         console.log("respJson: ", respJson);
//         //window.alert(respJson.length + ' tweets received.');
//         //Session.set("recentTweets", respJson);
//     }

//     });

//var imageData;
var results;


Template.home.rendered = function() {
    Session.set("Navigation", "report");
    Session.set("currentPage", "home");
    Session.set("loading", true);
    Session.set("dataExist", false);
    Session.set('isWeeklyReportEmpty', false);   //assuming weekly report has data
  
    //var url = 'https://api.parse.com/1/classes/ImageData';
    var url = imageDataUrl;
    var uname = '"' + Cookie.get('loggedInUser') + '"';
    var result = HTTP.get(url, {
            headers: {
                'X-Parse-Application-Id': appID,
                'X-Parse-REST-API-Key': restKey,
                'content-type': 'application/json'
            },
            params: {
                'order': "-createdAt",
                'limit':'900'
            },
            // query: queryString
            query: 'where={"ownername":' + uname + '}'


        },
        function(error, result) {
            Session.set("loading", false);
            if (error) {} else {


                var parsedContent = JSON.parse(result.content);
                results = parsedContent.results;
                for (var n = 0; n < results.length; n++) {
                    var setRDate = results[n];
                    //console.log(setRDate.date.iso);
                    var da = new Date(setRDate.date.iso);
                    setRDate.date.iso = da.toDateString() + " - " + da.toLocaleTimeString('en-US', {
                        hour12: true
                    });
                    //console.log(setRDate.date.iso);
                }
                if (results.length > 0)
                    Session.set("dataExist", true);
                Session.set("showList", false);
                Session.set('isWeekReport', false);
                imageData.set(results);
            }

        });

    // console.log(util.inspect(result.data, {depth: null}));

}

function filter(arr, criteria) {
    return arr.filter(function(obj) {
        return Object.keys(criteria).every(function(c) {
            return new RegExp(criteria[c]).test(obj[c]);
        });
    });
}
Template.home.helpers({

    getImageData: function() {
        //console.log("Owners: " + usersArr);
        return imageData.get();
    },

    getImageUrl: function(fileName){
        return "https://s3-us-west-1.amazonaws.com/waygroup/" + fileName;
    },

    loading: function() {
        return Session.get("loading");
    },

    dataExist: function() {
        return Session.get("dataExist");
    },
    showList: function() {
        return Session.get('showList');
    },
    isWeeklyReportEmpty: function() {
        return Session.get('isWeeklyReportEmpty');
    },

    results: function() {
        // console.log("Results: " + results);
        // for (var n = 0; n < results.length; n++) {
        //   //  console.log("UserName: " + results[n].UserName.length);
        //     if (results[n].UserName = "Usesr1") {

        //         var removedObject = results.splice(n, 1);

        //         removedObject = null;
        //         //break;
        //     }
        // }
        // console.log("Results: " + results);
        // return results;


        // results = filter(results, {
        //     UserName: Session.get("UserName")
        // });
        // console.log("Results: " + results);
        return results;
        //^ {age:21, color:'blue', name:'Louis}
    },
    PostId: function(objId) {
        if ((NotesList.find({
                Object_Id: objId
            })).count() > 0)
            return (NotesList.findOne({
                Object_Id: objId
            })).Notes;
    },
});

// Replace , with other character
// Remove /n
Template.home.events({

    // var $home = this.$('home');
    // var $table = home.$('myTable');
    'click #download': function(event, template) {
        Session.set("showList", true);
        Session.set('isWeeklyReportEmpty', false);
        // var note = replaceComma(rows[i].cells[totalCols - 1]);
        //  $("textarea").each(function(i) {
        //     notesArr.insert($(this).val())
        // })
        var days = 8; // Days you want to subtract
            var date = new Date();
            var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
            var minDate = Date.parse(last);
            var weeklyReport = [];
            for (var i = 0; i < results.length; i++) {

              //  var dateString = results[i].date.iso;
                var date = new Date(results[i].createdAt);
                var dateInSecs = date.getTime();
                // var isoDate = results[i].date.iso;
                // var resultDate = Date.parse(date);
                // var resultDateISO  = resultDate.iso;

                if ((dateInSecs) > minDate) {
                    weeklyReport.push(results[i]);
                }
            }
        if (weeklyReport.length == 0) {
            Session.set('isWeeklyReportEmpty', true);
        } else {
            Session.set('isWeeklyReportEmpty', false);
        }

    },
    'click #downloadAllReport': function(event, template) {
        Session.set("showList", false);
        var notes = template.findAll("textarea");

        var element = $('input[name="report"]:checked').val();
        if (element == 'ALLREPORT') {
            var i = 0;
            var yourCSVData = "USUARIO,FECHA,LUGAR,TÍTULO,DESCRIPCIÓN,FOTO,NOTAS\n";
            // results.forEach(function(result) {
            for (var n = 0; n < results.length; n++) {

                var note,username,place,date,description,imageUrl,title;
               // var note = replaceComma(notes[n].value);
                var result = results[n];
                //  var note = "Adfadsfad";
               // var username = replaceComma(result.username);
                //var place = replaceComma(result.location);
              //  var date = replaceComma(result.date.iso);
               //  var date = replaceComma(result.createdAt);
                //var description = replaceComma(result.description);
              //  var imageUrl = replaceComma(result.image.url);
                var title;
                if(typeof notes[n].value == 'undefined'){
                         note = '';
                }else{
                         note = replaceComma(notes[n].value);
                }

                if(typeof result.username == 'undefined'){
                         username = '';
                }else{
                         username = replaceComma(result.username);
                }

                if(typeof result.location == 'undefined'){
                         place = '';
                }else{
                         place = replaceComma(result.location);
                }

                 if(typeof result.createdAt == 'undefined'){
                         date = '';
                }else{
                         date = replaceComma(result.createdAt);
                }

                if(typeof result.description == 'undefined'){
                         description = '';
                }else{
                         description = replaceComma(result.description);
                }

                if(typeof result.image.url == 'undefined'){
                         imageUrl = '';
                }else{
                         imageUrl = replaceComma(result.image.url);
                }

                if(typeof result.title == 'undefined'){
                         title = '';
                }else{
                         title = replaceComma(result.title);
                }

             
               // var title = replaceComma(result.title);

                note = note.trim();
                username = username.trim();
                place = place.trim();
                date = date.trim();
                description = description.trim();
                imageUrl = imageUrl.trim();
                title = title.trim();

                yourCSVData = yourCSVData + username + "," + date + "," + place + "," + title + "," +
                    description + "," + imageUrl + "," + note + "\n";
            }
            //console.log("CSV DAta: " + yourCSVData);
            var blob = new Blob([yourCSVData], {
                type: "text/csv;charset=utf-8"
            });

            //console.log("Blob: " + blob);
            saveAs(blob, "Report.csv");
        } else {
         //   var date = new Date();
         //   var maxDate = Date.parse(date);
            var days = 8; // Days you want to subtract
            var date = new Date();
            var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
            var minDate = Date.parse(last);
            var weeklyReport = [];
            for (var i = 0; i < results.length; i++) {

              //  var dateString = results[i].date.iso;
                var date = new Date(results[i].createdAt);
                var dateInSecs = date.getTime();
                // var isoDate = results[i].date.iso;
                // var resultDate = Date.parse(date);
                // var resultDateISO  = resultDate.iso;

                if ((dateInSecs) > minDate) {
                    weeklyReport.push(results[i]);
                }
            }
            if (weeklyReport.length == 0) {
                alert("No data found for last week.");
            } else {
                var yourCSVData = "USUARIO,FECHA,LUGAR,TÍTULO,DESCRIPCIÓN,FOTO,NOTAS\n";
                // results.forEach(function(result) {
                for (var n = 0; n < weeklyReport.length; n++) {
                    var note = replaceComma(notes[n].value);
                    var result = weeklyReport[n];
                    //  var note = "Adfadsfad";
                    var username = replaceComma(result.username);
                    var place = replaceComma(result.location);
                    var date = replaceComma(result.date.iso);
                    var description = replaceComma(result.description);
                    var imageUrl = replaceComma(result.image.url);
                    var title = replaceComma(result.title);

                    note = note.trim();
                    username = username.trim();
                    place = place.trim();
                    date = date.trim();
                    description = description.trim();
                    imageUrl = imageUrl.trim();
                    title = title.trim();
                    //console.log("Note: " + note);
                    //console.log("Username: " + username);
                    // console.log("Place: " + place);
                    // console.log("Date: " + date);
                    // console.log("description: " + description);
                    // console.log("imageUrl: " + imageUrl);

                    yourCSVData = yourCSVData + username + "," + date + "," + place + "," + title + "," +
                        description + "," + imageUrl + "," + note + "\n";
                }
                //console.log("CSV DAta: " + yourCSVData);
                var blob = new Blob([yourCSVData], {
                    type: "text/csv;charset=utf-8"
                });

                //console.log("Blob: " + blob);
                saveAs(blob, "Report.csv");
            }
        }

    },
    'blur .myNotes': function() {
        var notes = $('#' + this.objectId).val();
        if (notes.length > 0) {
            Meteor.call('insertNotes', this.objectId, notes);
        }
    },
    'focus #inputRadio': function(event, template) {
        var element = $('input[name="report"]:checked').val();
        // console.log(element);
        // if (element !== 'WEEKREPORT') {
        //     var report = Session.get('isWeekReport');
        //     Session.set('isWeekReportNotEmpty', report);
        // } else {
        //     Session.set('isWeekReportNotEmpty', false);
        // }
    },
    'click #cancelBtn': function(event, template){
        Session.set("showList", false);
        // console.log('test');
    }


});
