// console.log("RouteName" + Router.current().route.getName());
//console.log("Meteor Users:" + Meteor.users.find().fetch());
Session.set("currentPage", "signin");

//This code is running properly
// Meteor.http.call("GET","http://api.openweathermap.org/data/2.5/weather?lat=35&lon=139",function(error,result)
// {
//         console.log("Webservice Call with Result: " + result.content);
// });

Session.set("error", false);
// function isFormValid(username, password) {
//     if ((password.length >= 5) && (username.length >= 5))
//     {
//          Session.set("error",false)
//          return true;
//     }
//     else
//     {
//         Session.set("error",true);
//         return false;
//     }
// }

/*
function loginUser(username, password) {
    Session.set("loading",true);
    //console.log("Username: " + username + " Password: " + password);
     var url = 'https://api.parse.com/1/login';
            var result =  HTTP.get(url, {
                headers: {
                    'X-Parse-Application-Id': appID,
                    'X-Parse-REST-API-Key': restKey,
                    'content-type': 'application/json'
                },
                params: {
                    'username': username,
                    'password': password
                }
            }, function(error, result) {
                   Session.set("loading",false);
                //console.log("Resut:" + result.content);
                if(error)
                    Session.set("error",true);
                else
                {
                    Cookie.set('loggedInUser', username)
                    //console.log("LoggedInUser: " + Cookie.get('loggedInUser'));
                    if(username == 'admin')
                        Router.go('registerOptions');
                    else{
                        Router.go('home');
                    }
                }
            });
}*/
function loginUser(username, password) {
    Session.set("loading",true);
    //console.log("Username: " + username + " Password: " + password);
    // var url = 'https://api.parse.com/1/login';
    var url = loginUrl;
            var result =  HTTP.get(url, {
                headers: {
                    'X-Parse-Application-Id': appID,
                    'X-Parse-REST-API-Key': restKey,
                    'content-type': 'application/json'
                },
                params: {
                    'username': username,
                    'password': password
                }
            }, function(error, result) {
                   Session.set("loading",false);
                //console.log("Resut:" + result.content);
                if(error)
                    Session.set("error",true);
                else
                {
                    Cookie.set('loggedInUser', username)
                    //console.log("LoggedInUser: " + Cookie.get('loggedInUser'));
                    if(username == 'admin')
                        Router.go('registerOptions');
                    else{
                        Router.go('home');
                    }
                }
            });
}

Template.signin.helpers({
    // trim helper
    trimInput: function(val) {
        return val.replace(/^\s*|\s*$/g, "");
    },


    error: function() {
        return Session.get("error");
    },

    loading:function()
    {
        return Session.get("loading");
    }


});

Template.signin.created = function() {
    //console.log("Signin Template created");
}

Template.signin.rendered = function() {
    Meteor.logout();
    Session.set("loading",false);
    Session.set("error", false);
    //console.log("SignIn Rendered");
    Session.set("currentPage", "Signin");

}
Template.signin.events({

    'submit': function(event, template) {
        event.preventDefault();
        Session.set("error",false);
        //console.log("submit EventType:" + event.type);
        //this.parseData();
        var username = template.find("#username").value;
        var password = template.find("#password").value;
        // console.log("Username: " + username + " Password: " + password);
        var isValid = isFormValid(username, password);
        if (isValid) {

            //console.log("Signin Form is valid");
            if(!Session.get("loading"))
               loginUser(username, password);

        } else {
            Session.set("error", true);
        }



        return false;
    }

   
});


function loginMongoDb(username, password) {
    Meteor.loginWithPassword(username, password, function(err) {
        if (err) {

            Session.set("error", true);
        }
        // The user might not have been found, or their passwword
        // could be incorrect. Inform the user that their
        // login attempt has failed. 
        else {
            //  Session.set("currentPage", "home");
            // Session.set("UserName", username);
            Router.go('registerOptions');
            //console.log("User Logged In: ");
        }
    });
}
